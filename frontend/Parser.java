//### This file created by BYACC 1.8(/Java extension  1.13)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//###           14 Sep 06  -- Keltin Leung-- ReduceListener support, eliminate underflow report in error recovery
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 11 "Parser.y"
package decaf.frontend;

import decaf.tree.Tree;
import decaf.tree.Tree.*;
import decaf.error.*;
import java.util.*;
//#line 25 "Parser.java"
interface ReduceListener {
  public boolean onReduce(String rule);
}




public class Parser
             extends BaseParser
             implements ReduceListener
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

ReduceListener reduceListener = null;
void yyclearin ()       {yychar = (-1);}
void yyerrok ()         {yyerrflag=0;}
void addReduceListener(ReduceListener l) {
  reduceListener = l;}


//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:SemValue
String   yytext;//user variable to return contextual strings
SemValue yyval; //used to return semantic vals from action routines
SemValue yylval;//the 'lval' (result) I got from yylex()
SemValue valstk[] = new SemValue[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new SemValue();
  yylval=new SemValue();
  valptr=-1;
}
final void val_push(SemValue val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    SemValue[] newstack = new SemValue[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final SemValue val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final SemValue val_peek(int relative)
{
  return valstk[valptr-relative];
}
//#### end semantic value section ####
public final static short VOID=257;
public final static short INT=258;
public final static short BOOL=259;
public final static short DOUBLE=260;
public final static short STRING=261;
public final static short CLASS=262;
public final static short NULL=263;
public final static short EXTENDS=264;
public final static short THIS=265;
public final static short WHILE=266;
public final static short FOR=267;
public final static short REPEAT=268;
public final static short UNTIL=269;
public final static short IF=270;
public final static short ELSE=271;
public final static short RETURN=272;
public final static short BREAK=273;
public final static short NEW=274;
public final static short PRINT=275;
public final static short READ_INTEGER=276;
public final static short READ_LINE=277;
public final static short LITERAL=278;
public final static short IDENTIFIER=279;
public final static short AND=280;
public final static short OR=281;
public final static short STATIC=282;
public final static short LESS_EQUAL=283;
public final static short GREATER_EQUAL=284;
public final static short EQUAL=285;
public final static short NOT_EQUAL=286;
public final static short UMINUS=287;
public final static short EMPTY=288;
public final static short INSTANCEOF=289;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    1,    1,    3,    4,    5,    5,    5,    5,    5,
    5,    5,    2,    6,    6,    7,    7,    7,    9,    9,
   10,   10,    8,    8,   11,   12,   12,   13,   13,   13,
   13,   13,   13,   13,   13,   13,   13,   14,   14,   14,
   25,   25,   22,   22,   24,   23,   23,   23,   23,   23,
   23,   23,   23,   23,   23,   23,   23,   23,   23,   23,
   23,   23,   23,   23,   23,   23,   23,   23,   23,   23,
   23,   27,   27,   26,   26,   28,   28,   16,   17,   18,
   21,   15,   29,   29,   19,   19,   20,
};
final static short yylen[] = {                            2,
    1,    2,    1,    2,    2,    1,    1,    1,    1,    1,
    2,    3,    6,    2,    0,    2,    2,    0,    1,    0,
    3,    1,    7,    6,    3,    2,    0,    1,    2,    1,
    1,    2,    1,    2,    2,    2,    1,    3,    1,    0,
    2,    0,    2,    4,    5,    1,    1,    1,    3,    3,
    3,    3,    3,    3,    3,    3,    3,    3,    3,    3,
    3,    3,    2,    2,    3,    3,    1,    4,    5,    6,
    5,    1,    1,    1,    0,    3,    1,    5,    6,    9,
    1,    6,    2,    0,    2,    1,    4,
};
final static short yydefred[] = {                         0,
    0,    0,    0,    3,    0,    2,    0,    0,   14,   18,
    0,    7,    6,    8,    9,   10,    0,    0,   13,   16,
    0,    0,   17,   11,    0,    4,    0,    0,    0,    0,
   12,    0,   22,    0,    0,    0,    0,    5,    0,    0,
    0,   27,   24,   21,   23,    0,   73,   67,    0,    0,
    0,    0,    0,   81,    0,    0,    0,    0,   72,    0,
    0,    0,   25,    0,   28,   37,   26,    0,   30,   31,
    0,   33,    0,    0,    0,    0,    0,    0,    0,   48,
    0,    0,    0,    0,   46,    0,   47,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   29,   32,   34,
   35,   36,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   41,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   65,   66,    0,
   62,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   68,    0,    0,   87,    0,    0,   44,    0,
    0,   78,    0,    0,    0,   69,    0,   71,    0,   45,
    0,   79,    0,   82,   70,    0,   83,    0,   80,
};
final static short yydgoto[] = {                          2,
    3,    4,   65,   21,   34,    8,   11,   23,   35,   36,
   66,   46,   67,   68,   69,   70,   71,   72,   73,   74,
   75,   85,   77,   87,   79,  160,   80,  127,  174,
};
final static short yysindex[] = {                      -257,
 -266,    0, -257,    0, -246,    0, -255,  -95,    0,    0,
  106,    0,    0,    0,    0,    0, -250,  -52,    0,    0,
  -12,  -87,    0,    0,  -85,    0,   16,  -60,   20,  -52,
    0,  -52,    0,  -77,   13,   26,   23,    0,  -51,  -52,
  -51,    0,    0,    0,    0,   -8,    0,    0,   35,   36,
   17,   37,   56,    0, -216,   38,   41,   42,    0,   56,
   56,   34,    0,   46,    0,    0,    0,   28,    0,    0,
   29,    0,   32,   39,   43,   33,  510,    0, -186,    0,
   56,   56, -174,   56,    0,  510,    0,   59,   12,   56,
   71,   72,  -43,  -43, -163,  381,   56,    0,    0,    0,
    0,    0,   56,   56,   56,   56,   56,   56,   56,   56,
   56,   56,   56,   56,   56,   56,    0,   56,   78,  405,
   60,   80,  416,   86,   40,  510,   14,    0,    0,   87,
    0,  437,  510,  582,  561,  -26,  -26,  612,  612,   -7,
   -7,  -43,  -43,  -43,  -26,  -26,  448,   56,   17,   56,
   56,   17,    0,  469,   56,    0,   56, -158,    0,   97,
   95,    0,  476,  503, -125,    0,  510,    0,  116,    0,
   56,    0,   17,    0,    0,  117,    0,   17,    0,
};
final static short yyrindex[] = {                         0,
    0,    0,  164,    0,   49,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  114,    0,    0,  134,
    0,  134,    0,    0,    0,  135,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  -58,    0,    0,    0,    0,
  -58,    0,  -57,    0,    0,    0,    0,    0,    0, -102,
 -102, -102,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  550,    0,   89,    0,    0,
 -102,  -58,    0, -102,    0,  119,    0,    0,    0, -102,
    0,    0,  100,  124,    0,    0, -102,    0,    0,    0,
    0,    0, -102, -102, -102, -102, -102, -102, -102, -102,
 -102, -102, -102, -102, -102, -102,    0, -102,   63,    0,
    0,    0,    0,    0, -102,   25,    0,    0,    0,    0,
    0,    0,  -32,   70,  -10,  461,  483,  111,  541,  665,
  756,  154,  294,  352,  525,  527,    0,  -31,  -58, -102,
 -102,  -58,    0,    0, -102,    0, -102,    0,    0,    0,
  140,    0,    0,    0,  -33,    0,   27,    0,    0,    0,
  -18,    0,  -58,    0,    0,    0,    0,  -58,    0,
};
final static short yygindex[] = {                         0,
    0,  179,  174,   21,    4,    0,    0,    0,  155,    0,
   -3,    0,    1,  -74,    0,    0,    0,    0,    0,    0,
    0,  599,  818,  720,    0,    0,    0,   52,    0,
};
final static int YYTABLESIZE=1042;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         84,
   40,   86,  117,   28,    1,   28,   84,  121,   38,   75,
  114,   84,    5,   28,   22,  112,  110,    7,  111,  117,
  113,   25,   40,    9,   61,   84,   38,   10,   24,  114,
   61,   62,   31,   61,  112,   43,   60,   45,  117,  113,
   12,   13,   14,   15,   16,   17,   26,  118,   61,   61,
   33,   83,   33,   39,  156,   30,   62,  155,   89,   32,
   44,   60,   88,   41,  118,   77,   61,   76,   77,   40,
   76,   42,   61,   62,   81,   82,   84,   90,   60,   62,
   91,   92,   61,  118,   60,   97,   98,   99,   61,   84,
  100,   84,  119,  103,  122,   62,  176,  101,  124,   43,
   60,  102,  125,   43,   43,   43,   43,   43,   43,   43,
   60,  128,  129,   60,   42,  130,   63,  148,  150,  151,
  169,   43,   43,   43,   43,   47,  153,  157,   60,   39,
   47,   47,   31,   47,   47,   47,   63,  170,  155,   42,
   63,   63,   63,   63,   63,  173,   63,   39,   47,  162,
   47,   54,  165,   43,   54,   43,  175,  178,   63,   63,
   64,   63,   60,    1,   64,   64,   64,   64,   64,   54,
   64,   15,    5,  177,   20,   19,   42,   85,  179,   47,
   74,    6,   64,   64,   20,   64,   37,    0,    0,    0,
   51,   27,   63,   29,   51,   51,   51,   51,   51,  161,
   51,   38,    0,   54,   12,   13,   14,   15,   16,   17,
    0,    0,   51,   51,    0,   51,   64,    0,    0,    0,
   42,   42,    0,   84,   84,   84,   84,   84,   84,   84,
   19,   84,   84,   84,   84,   84,   84,    0,   84,   84,
   84,   84,   84,   84,   84,   84,   51,   42,   12,   13,
   14,   15,   16,   17,   47,   84,   48,   49,   50,   51,
   42,   52,    0,   53,   54,   55,   56,   57,   58,   59,
   61,    0,    0,   12,   13,   14,   15,   16,   17,   47,
   64,   48,   49,   50,   51,    0,   52,    0,   53,   54,
   55,   56,   57,   58,   59,   95,   47,    0,   48,    0,
    0,    0,   47,    0,   48,   64,    0,   55,    0,   57,
   58,   59,    0,   55,    0,   57,   58,   59,   47,    0,
   48,    0,   64,    0,    0,    0,    0,    0,   64,   55,
   52,   57,   58,   59,   52,   52,   52,   52,   52,    0,
   52,    0,   43,   43,   64,   43,   43,   43,   43,   60,
   60,    0,   52,   52,    0,   52,    0,    0,    0,    0,
    0,    0,   12,   13,   14,   15,   16,   17,   47,   47,
    0,   47,   47,   47,   47,    0,    0,    0,    0,   63,
   63,    0,   63,   63,   63,   63,   52,   18,   53,    0,
   54,   54,   53,   53,   53,   53,   53,    0,   53,    0,
    0,    0,    0,   64,   64,    0,   64,   64,   64,   64,
   53,   53,    0,   53,    0,    0,    0,  114,    0,    0,
    0,  131,  112,  110,    0,  111,  117,  113,    0,    0,
    0,    0,    0,   51,   51,    0,   51,   51,   51,   51,
  116,  114,  115,    0,   53,  149,  112,  110,    0,  111,
  117,  113,  114,    0,    0,    0,  152,  112,  110,    0,
  111,  117,  113,    0,  116,    0,  115,    0,    0,    0,
    0,  118,    0,  114,    0,  116,    0,  115,  112,  110,
  158,  111,  117,  113,  114,    0,    0,    0,    0,  112,
  110,    0,  111,  117,  113,  118,  116,    0,  115,    0,
    0,   58,    0,    0,   58,  114,  118,  116,    0,  115,
  112,  110,  114,  111,  117,  113,    0,  112,  110,   58,
  111,  117,  113,   59,    0,    0,   59,  118,  116,    0,
  115,    0,    0,    0,  171,  116,    0,  115,  118,  114,
  159,   59,    0,  172,  112,  110,  114,  111,  117,  113,
    0,  112,  110,   58,  111,  117,  113,    0,    0,  118,
    0,  166,  116,    0,  115,   57,  118,   56,   57,  116,
   56,  115,    0,   52,   52,   59,   52,   52,   52,   52,
    0,   55,    0,   57,   55,   56,   46,    0,    0,    0,
    0,   46,   46,  118,   46,   46,   46,  114,    0,   55,
  118,    0,  112,  110,    0,  111,  117,  113,    0,   46,
    0,   46,    0,    0,    0,    0,    0,   57,  114,   56,
  116,    0,  115,  112,  110,    0,  111,  117,  113,    0,
    0,   53,   53,   55,   53,   53,   53,   53,    0,    0,
   46,  116,    0,  115,   76,    0,    0,    0,  114,   76,
    0,  118,    0,  112,  110,    0,  111,  117,  113,    0,
  104,  105,    0,  106,  107,  108,  109,    0,    0,    0,
    0,  116,  118,  115,    0,    0,    0,    0,    0,    0,
   76,    0,    0,    0,  104,  105,    0,  106,  107,  108,
  109,    0,    0,    0,    0,  104,  105,    0,  106,  107,
  108,  109,  118,    0,    0,   49,    0,   49,   49,   49,
    0,    0,    0,    0,    0,    0,  104,  105,    0,  106,
  107,  108,  109,   49,   49,    0,   49,  104,  105,    0,
  106,  107,  108,  109,    0,    0,    0,    0,    0,    0,
   58,   58,    0,    0,    0,   58,   58,   76,  104,  105,
   76,  106,  107,  108,  109,  104,  105,   49,  106,  107,
  108,  109,   59,   59,    0,   78,    0,   59,   59,   76,
   78,   76,    0,    0,    0,    0,   76,    0,    0,    0,
    0,    0,  104,  105,    0,  106,  107,  108,  109,  104,
  105,    0,  106,  107,  108,  109,   50,    0,   50,   50,
   50,   78,    0,    0,   57,   57,   56,   56,    0,   57,
   57,   56,   56,    0,   50,   50,    0,   50,    0,    0,
   55,   55,    0,    0,    0,    0,    0,    0,    0,   46,
   46,    0,   46,   46,   46,   46,    0,    0,    0,    0,
  104,    0,    0,  106,  107,  108,  109,    0,   50,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  106,  107,  108,  109,   78,    0,
   86,   78,    0,    0,    0,    0,    0,   93,   94,   96,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   78,    0,   78,    0,  106,  107,    0,   78,  120,    0,
    0,  123,    0,    0,    0,    0,    0,  126,    0,    0,
    0,    0,    0,    0,  132,    0,    0,    0,    0,    0,
  133,  134,  135,  136,  137,  138,  139,  140,  141,  142,
  143,  144,  145,  146,    0,  147,    0,    0,    0,    0,
    0,    0,  154,    0,   49,   49,    0,   49,   49,   49,
   49,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  126,    0,  163,  164,    0,
    0,    0,  167,    0,  168,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   50,   50,    0,   50,   50,
   50,   50,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         33,
   59,   59,   46,   91,  262,   91,   40,   82,   41,   41,
   37,   45,  279,   91,   11,   42,   43,  264,   45,   46,
   47,   18,   41,  279,   33,   59,   59,  123,  279,   37,
   41,   40,   93,   44,   42,   39,   45,   41,   46,   47,
  257,  258,  259,  260,  261,  262,   59,   91,   59,   33,
   30,   51,   32,   41,   41,   40,   40,   44,   55,   40,
   40,   45,  279,   41,   91,   41,   33,   41,   44,   44,
   44,  123,   33,   40,   40,   40,   40,   40,   45,   40,
   40,   40,   93,   91,   45,   40,   59,   59,   33,  123,
   59,  125,  279,   61,  269,   40,  171,   59,   40,   37,
   45,   59,   91,   41,   42,   43,   44,   45,   46,   47,
   41,   41,   41,   44,  123,  279,  125,   40,   59,   40,
  279,   59,   60,   61,   62,   37,   41,   41,   59,   41,
   42,   43,   93,   45,   46,   47,   37,   41,   44,  123,
   41,   42,   43,   44,   45,  271,   47,   59,   60,  149,
   62,   41,  152,   91,   44,   93,   41,   41,   59,   60,
   37,   62,   93,    0,   41,   42,   43,   44,   45,   59,
   47,  123,   59,  173,   41,   41,  279,   59,  178,   91,
   41,    3,   59,   60,   11,   62,   32,   -1,   -1,   -1,
   37,  279,   93,  279,   41,   42,   43,   44,   45,  148,
   47,  279,   -1,   93,  257,  258,  259,  260,  261,  262,
   -1,   -1,   59,   60,   -1,   62,   93,   -1,   -1,   -1,
  279,  279,   -1,  257,  258,  259,  260,  261,  262,  263,
  125,  265,  266,  267,  268,  269,  270,   -1,  272,  273,
  274,  275,  276,  277,  278,  279,   93,  279,  257,  258,
  259,  260,  261,  262,  263,  289,  265,  266,  267,  268,
  279,  270,   -1,  272,  273,  274,  275,  276,  277,  278,
  281,   -1,   -1,  257,  258,  259,  260,  261,  262,  263,
  289,  265,  266,  267,  268,   -1,  270,   -1,  272,  273,
  274,  275,  276,  277,  278,  262,  263,   -1,  265,   -1,
   -1,   -1,  263,   -1,  265,  289,   -1,  274,   -1,  276,
  277,  278,   -1,  274,   -1,  276,  277,  278,  263,   -1,
  265,   -1,  289,   -1,   -1,   -1,   -1,   -1,  289,  274,
   37,  276,  277,  278,   41,   42,   43,   44,   45,   -1,
   47,   -1,  280,  281,  289,  283,  284,  285,  286,  280,
  281,   -1,   59,   60,   -1,   62,   -1,   -1,   -1,   -1,
   -1,   -1,  257,  258,  259,  260,  261,  262,  280,  281,
   -1,  283,  284,  285,  286,   -1,   -1,   -1,   -1,  280,
  281,   -1,  283,  284,  285,  286,   93,  282,   37,   -1,
  280,  281,   41,   42,   43,   44,   45,   -1,   47,   -1,
   -1,   -1,   -1,  280,  281,   -1,  283,  284,  285,  286,
   59,   60,   -1,   62,   -1,   -1,   -1,   37,   -1,   -1,
   -1,   41,   42,   43,   -1,   45,   46,   47,   -1,   -1,
   -1,   -1,   -1,  280,  281,   -1,  283,  284,  285,  286,
   60,   37,   62,   -1,   93,   41,   42,   43,   -1,   45,
   46,   47,   37,   -1,   -1,   -1,   41,   42,   43,   -1,
   45,   46,   47,   -1,   60,   -1,   62,   -1,   -1,   -1,
   -1,   91,   -1,   37,   -1,   60,   -1,   62,   42,   43,
   44,   45,   46,   47,   37,   -1,   -1,   -1,   -1,   42,
   43,   -1,   45,   46,   47,   91,   60,   -1,   62,   -1,
   -1,   41,   -1,   -1,   44,   37,   91,   60,   -1,   62,
   42,   43,   37,   45,   46,   47,   -1,   42,   43,   59,
   45,   46,   47,   41,   -1,   -1,   44,   91,   60,   -1,
   62,   -1,   -1,   -1,   59,   60,   -1,   62,   91,   37,
   93,   59,   -1,   41,   42,   43,   37,   45,   46,   47,
   -1,   42,   43,   93,   45,   46,   47,   -1,   -1,   91,
   -1,   93,   60,   -1,   62,   41,   91,   41,   44,   60,
   44,   62,   -1,  280,  281,   93,  283,  284,  285,  286,
   -1,   41,   -1,   59,   44,   59,   37,   -1,   -1,   -1,
   -1,   42,   43,   91,   45,   46,   47,   37,   -1,   59,
   91,   -1,   42,   43,   -1,   45,   46,   47,   -1,   60,
   -1,   62,   -1,   -1,   -1,   -1,   -1,   93,   37,   93,
   60,   -1,   62,   42,   43,   -1,   45,   46,   47,   -1,
   -1,  280,  281,   93,  283,  284,  285,  286,   -1,   -1,
   91,   60,   -1,   62,   46,   -1,   -1,   -1,   37,   51,
   -1,   91,   -1,   42,   43,   -1,   45,   46,   47,   -1,
  280,  281,   -1,  283,  284,  285,  286,   -1,   -1,   -1,
   -1,   60,   91,   62,   -1,   -1,   -1,   -1,   -1,   -1,
   82,   -1,   -1,   -1,  280,  281,   -1,  283,  284,  285,
  286,   -1,   -1,   -1,   -1,  280,  281,   -1,  283,  284,
  285,  286,   91,   -1,   -1,   41,   -1,   43,   44,   45,
   -1,   -1,   -1,   -1,   -1,   -1,  280,  281,   -1,  283,
  284,  285,  286,   59,   60,   -1,   62,  280,  281,   -1,
  283,  284,  285,  286,   -1,   -1,   -1,   -1,   -1,   -1,
  280,  281,   -1,   -1,   -1,  285,  286,  149,  280,  281,
  152,  283,  284,  285,  286,  280,  281,   93,  283,  284,
  285,  286,  280,  281,   -1,   46,   -1,  285,  286,  171,
   51,  173,   -1,   -1,   -1,   -1,  178,   -1,   -1,   -1,
   -1,   -1,  280,  281,   -1,  283,  284,  285,  286,  280,
  281,   -1,  283,  284,  285,  286,   41,   -1,   43,   44,
   45,   82,   -1,   -1,  280,  281,  280,  281,   -1,  285,
  286,  285,  286,   -1,   59,   60,   -1,   62,   -1,   -1,
  280,  281,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  280,
  281,   -1,  283,  284,  285,  286,   -1,   -1,   -1,   -1,
  280,   -1,   -1,  283,  284,  285,  286,   -1,   93,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  283,  284,  285,  286,  149,   -1,
   53,  152,   -1,   -1,   -1,   -1,   -1,   60,   61,   62,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  171,   -1,  173,   -1,  283,  284,   -1,  178,   81,   -1,
   -1,   84,   -1,   -1,   -1,   -1,   -1,   90,   -1,   -1,
   -1,   -1,   -1,   -1,   97,   -1,   -1,   -1,   -1,   -1,
  103,  104,  105,  106,  107,  108,  109,  110,  111,  112,
  113,  114,  115,  116,   -1,  118,   -1,   -1,   -1,   -1,
   -1,   -1,  125,   -1,  280,  281,   -1,  283,  284,  285,
  286,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  148,   -1,  150,  151,   -1,
   -1,   -1,  155,   -1,  157,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  280,  281,   -1,  283,  284,
  285,  286,
};
}
final static short YYFINAL=2;
final static short YYMAXTOKEN=289;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"'!'",null,null,null,"'%'",null,null,"'('","')'","'*'","'+'",
"','","'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,null,
"';'","'<'","'='","'>'",null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,"'['",null,"']'",null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,"'{'",null,"'}'",null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,"VOID","INT","BOOL","DOUBLE",
"STRING","CLASS","NULL","EXTENDS","THIS","WHILE","FOR","REPEAT","UNTIL","IF",
"ELSE","RETURN","BREAK","NEW","PRINT","READ_INTEGER","READ_LINE","LITERAL",
"IDENTIFIER","AND","OR","STATIC","LESS_EQUAL","GREATER_EQUAL","EQUAL",
"NOT_EQUAL","UMINUS","EMPTY","INSTANCEOF",
};
final static String yyrule[] = {
"$accept : Program",
"Program : ClassList",
"ClassList : ClassList ClassDef",
"ClassList : ClassDef",
"VariableDef : Variable ';'",
"Variable : Type IDENTIFIER",
"Type : INT",
"Type : VOID",
"Type : BOOL",
"Type : DOUBLE",
"Type : STRING",
"Type : CLASS IDENTIFIER",
"Type : Type '[' ']'",
"ClassDef : CLASS IDENTIFIER ExtendsClause '{' FieldList '}'",
"ExtendsClause : EXTENDS IDENTIFIER",
"ExtendsClause :",
"FieldList : FieldList VariableDef",
"FieldList : FieldList FunctionDef",
"FieldList :",
"Formals : VariableList",
"Formals :",
"VariableList : VariableList ',' Variable",
"VariableList : Variable",
"FunctionDef : STATIC Type IDENTIFIER '(' Formals ')' StmtBlock",
"FunctionDef : Type IDENTIFIER '(' Formals ')' StmtBlock",
"StmtBlock : '{' StmtList '}'",
"StmtList : StmtList Stmt",
"StmtList :",
"Stmt : VariableDef",
"Stmt : SimpleStmt ';'",
"Stmt : IfStmt",
"Stmt : WhileStmt",
"Stmt : RepeatStmt ';'",
"Stmt : ForStmt",
"Stmt : ReturnStmt ';'",
"Stmt : PrintStmt ';'",
"Stmt : BreakStmt ';'",
"Stmt : StmtBlock",
"SimpleStmt : LValue '=' Expr",
"SimpleStmt : Call",
"SimpleStmt :",
"Receiver : Expr '.'",
"Receiver :",
"LValue : Receiver IDENTIFIER",
"LValue : Expr '[' Expr ']'",
"Call : Receiver IDENTIFIER '(' Actuals ')'",
"Expr : LValue",
"Expr : Call",
"Expr : Constant",
"Expr : Expr '+' Expr",
"Expr : Expr '-' Expr",
"Expr : Expr '*' Expr",
"Expr : Expr '/' Expr",
"Expr : Expr '%' Expr",
"Expr : Expr EQUAL Expr",
"Expr : Expr NOT_EQUAL Expr",
"Expr : Expr '<' Expr",
"Expr : Expr '>' Expr",
"Expr : Expr LESS_EQUAL Expr",
"Expr : Expr GREATER_EQUAL Expr",
"Expr : Expr AND Expr",
"Expr : Expr OR Expr",
"Expr : '(' Expr ')'",
"Expr : '-' Expr",
"Expr : '!' Expr",
"Expr : READ_INTEGER '(' ')'",
"Expr : READ_LINE '(' ')'",
"Expr : THIS",
"Expr : NEW IDENTIFIER '(' ')'",
"Expr : NEW Type '[' Expr ']'",
"Expr : INSTANCEOF '(' Expr ',' IDENTIFIER ')'",
"Expr : '(' CLASS IDENTIFIER ')' Expr",
"Constant : LITERAL",
"Constant : NULL",
"Actuals : ExprList",
"Actuals :",
"ExprList : ExprList ',' Expr",
"ExprList : Expr",
"WhileStmt : WHILE '(' Expr ')' Stmt",
"RepeatStmt : REPEAT Stmt UNTIL '(' Expr ')'",
"ForStmt : FOR '(' SimpleStmt ';' Expr ';' SimpleStmt ')' Stmt",
"BreakStmt : BREAK",
"IfStmt : IF '(' Expr ')' Stmt ElseClause",
"ElseClause : ELSE Stmt",
"ElseClause :",
"ReturnStmt : RETURN Expr",
"ReturnStmt : RETURN",
"PrintStmt : PRINT '(' ExprList ')'",
};

//#line 435 "Parser.y"
    
	/**
	 * 打印当前归约所用的语法规则<br>
	 * 请勿修改。
	 */
    public boolean onReduce(String rule) {
		if (rule.startsWith("$$"))
			return false;
		else
			rule = rule.replaceAll(" \\$\\$\\d+", "");

   	    if (rule.endsWith(":"))
    	    System.out.println(rule + " <empty>");
   	    else
			System.out.println(rule);
		return false;
    }
    
    public void diagnose() {
		addReduceListener(this);
		yyparse();
	}
//#line 592 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    //if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      //if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        //if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        //if (yychar < 0)    //it it didn't work/error
        //  {
        //  yychar = 0;      //change it to default string (no -1!)
          //if (yydebug)
          //  yylexdebug(yystate,yychar);
        //  }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        //if (yydebug)
          //debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      //if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0 || valptr<0)   //check for under & overflow here
            {
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            //if (yydebug)
              //debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            //if (yydebug)
              //debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0 || valptr<0)   //check for under & overflow here
              {
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        //if (yydebug)
          //{
          //yys = null;
          //if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          //if (yys == null) yys = "illegal-symbol";
          //debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          //}
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    //if (yydebug)
      //debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    if (reduceListener == null || reduceListener.onReduce(yyrule[yyn])) // if intercepted!
      switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 53 "Parser.y"
{
						tree = new Tree.TopLevel(val_peek(0).clist, val_peek(0).loc);
					}
break;
case 2:
//#line 59 "Parser.y"
{
						yyval.clist.add(val_peek(0).cdef);
					}
break;
case 3:
//#line 63 "Parser.y"
{
                		yyval.clist = new ArrayList<Tree.ClassDef>();
                		yyval.clist.add(val_peek(0).cdef);
                	}
break;
case 5:
//#line 73 "Parser.y"
{
						yyval.vdef = new Tree.VarDef(val_peek(0).ident, val_peek(1).type, val_peek(0).loc);
					}
break;
case 6:
//#line 79 "Parser.y"
{
						yyval.type = new Tree.TypeIdent(Tree.INT, val_peek(0).loc);
					}
break;
case 7:
//#line 83 "Parser.y"
{
                		yyval.type = new Tree.TypeIdent(Tree.VOID, val_peek(0).loc);
                	}
break;
case 8:
//#line 87 "Parser.y"
{
                		yyval.type = new Tree.TypeIdent(Tree.BOOL, val_peek(0).loc);
                	}
break;
case 9:
//#line 91 "Parser.y"
{
                        yyval.type = new Tree.TypeIdent(Tree.DOUBLE, val_peek(0).loc);
                    }
break;
case 10:
//#line 95 "Parser.y"
{
                		yyval.type = new Tree.TypeIdent(Tree.STRING, val_peek(0).loc);
                	}
break;
case 11:
//#line 99 "Parser.y"
{
                		yyval.type = new Tree.TypeClass(val_peek(0).ident, val_peek(1).loc);
                	}
break;
case 12:
//#line 103 "Parser.y"
{
                		yyval.type = new Tree.TypeArray(val_peek(2).type, val_peek(2).loc);
                	}
break;
case 13:
//#line 109 "Parser.y"
{
						yyval.cdef = new Tree.ClassDef(val_peek(4).ident, val_peek(3).ident, val_peek(1).flist, val_peek(5).loc);
					}
break;
case 14:
//#line 115 "Parser.y"
{
						yyval.ident = val_peek(0).ident;
					}
break;
case 15:
//#line 119 "Parser.y"
{
                		yyval = new SemValue();
                	}
break;
case 16:
//#line 125 "Parser.y"
{
						yyval.flist.add(val_peek(0).vdef);
					}
break;
case 17:
//#line 129 "Parser.y"
{
						yyval.flist.add(val_peek(0).fdef);
					}
break;
case 18:
//#line 133 "Parser.y"
{
                		yyval = new SemValue();
                		yyval.flist = new ArrayList<Tree>();
                	}
break;
case 20:
//#line 141 "Parser.y"
{
                		yyval = new SemValue();
                		yyval.vlist = new ArrayList<Tree.VarDef>(); 
                	}
break;
case 21:
//#line 148 "Parser.y"
{
						yyval.vlist.add(val_peek(0).vdef);
					}
break;
case 22:
//#line 152 "Parser.y"
{
                		yyval.vlist = new ArrayList<Tree.VarDef>();
						yyval.vlist.add(val_peek(0).vdef);
                	}
break;
case 23:
//#line 159 "Parser.y"
{
						yyval.fdef = new MethodDef(true, val_peek(4).ident, val_peek(5).type, val_peek(2).vlist, (Block) val_peek(0).stmt, val_peek(4).loc);
					}
break;
case 24:
//#line 163 "Parser.y"
{
						yyval.fdef = new MethodDef(false, val_peek(4).ident, val_peek(5).type, val_peek(2).vlist, (Block) val_peek(0).stmt, val_peek(4).loc);
					}
break;
case 25:
//#line 169 "Parser.y"
{
						yyval.stmt = new Block(val_peek(1).slist, val_peek(2).loc);
					}
break;
case 26:
//#line 175 "Parser.y"
{
						yyval.slist.add(val_peek(0).stmt);
					}
break;
case 27:
//#line 179 "Parser.y"
{
                		yyval = new SemValue();
                		yyval.slist = new ArrayList<Tree>();
                	}
break;
case 28:
//#line 186 "Parser.y"
{
						yyval.stmt = val_peek(0).vdef;
					}
break;
case 29:
//#line 191 "Parser.y"
{
                		if (yyval.stmt == null) {
                			yyval.stmt = new Tree.Skip(val_peek(0).loc);
                		}
                	}
break;
case 38:
//#line 207 "Parser.y"
{
						yyval.stmt = new Tree.Assign(val_peek(2).lvalue, val_peek(0).expr, val_peek(1).loc);
					}
break;
case 39:
//#line 211 "Parser.y"
{
                		yyval.stmt = new Tree.Exec(val_peek(0).expr, val_peek(0).loc);
                	}
break;
case 40:
//#line 215 "Parser.y"
{
                		yyval = new SemValue();
                	}
break;
case 42:
//#line 222 "Parser.y"
{
                		yyval = new SemValue();
                	}
break;
case 43:
//#line 228 "Parser.y"
{
						yyval.lvalue = new Tree.Ident(val_peek(1).expr, val_peek(0).ident, val_peek(0).loc);
						if (val_peek(1).loc == null) {
							yyval.loc = val_peek(0).loc;
						}
					}
break;
case 44:
//#line 235 "Parser.y"
{
                		yyval.lvalue = new Tree.Indexed(val_peek(3).expr, val_peek(1).expr, val_peek(3).loc);
                	}
break;
case 45:
//#line 241 "Parser.y"
{
						yyval.expr = new Tree.CallExpr(val_peek(4).expr, val_peek(3).ident, val_peek(1).elist, val_peek(3).loc);
						if (val_peek(4).loc == null) {
							yyval.loc = val_peek(3).loc;
						}
					}
break;
case 46:
//#line 250 "Parser.y"
{
						yyval.expr = val_peek(0).lvalue;
					}
break;
case 49:
//#line 256 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.PLUS, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 50:
//#line 260 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.MINUS, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 51:
//#line 264 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.MUL, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 52:
//#line 268 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.DIV, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 53:
//#line 272 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.MOD, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 54:
//#line 276 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.EQ, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 55:
//#line 280 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.NE, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 56:
//#line 284 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.LT, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 57:
//#line 288 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.GT, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 58:
//#line 292 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.LE, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 59:
//#line 296 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.GE, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 60:
//#line 300 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.AND, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 61:
//#line 304 "Parser.y"
{
                		yyval.expr = new Tree.Binary(Tree.OR, val_peek(2).expr, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 62:
//#line 308 "Parser.y"
{
                		yyval = val_peek(1);
                	}
break;
case 63:
//#line 312 "Parser.y"
{
                		yyval.expr = new Tree.Unary(Tree.NEG, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 64:
//#line 316 "Parser.y"
{
                		yyval.expr = new Tree.Unary(Tree.NOT, val_peek(0).expr, val_peek(1).loc);
                	}
break;
case 65:
//#line 320 "Parser.y"
{
                		yyval.expr = new Tree.ReadIntExpr(val_peek(2).loc);
                	}
break;
case 66:
//#line 324 "Parser.y"
{
                		yyval.expr = new Tree.ReadLineExpr(val_peek(2).loc);
                	}
break;
case 67:
//#line 328 "Parser.y"
{
                		yyval.expr = new Tree.ThisExpr(val_peek(0).loc);
                	}
break;
case 68:
//#line 332 "Parser.y"
{
                		yyval.expr = new Tree.NewClass(val_peek(2).ident, val_peek(3).loc);
                	}
break;
case 69:
//#line 336 "Parser.y"
{
                		yyval.expr = new Tree.NewArray(val_peek(3).type, val_peek(1).expr, val_peek(4).loc);
                	}
break;
case 70:
//#line 340 "Parser.y"
{
                		yyval.expr = new Tree.TypeTest(val_peek(3).expr, val_peek(1).ident, val_peek(5).loc);
                	}
break;
case 71:
//#line 344 "Parser.y"
{
                		yyval.expr = new Tree.TypeCast(val_peek(2).ident, val_peek(0).expr, val_peek(0).loc);
                	}
break;
case 72:
//#line 350 "Parser.y"
{
						yyval.expr = new Tree.Literal(val_peek(0).typeTag, val_peek(0).literal, val_peek(0).loc);
					}
break;
case 73:
//#line 354 "Parser.y"
{
						yyval.expr = new Null(val_peek(0).loc);
					}
break;
case 75:
//#line 361 "Parser.y"
{
                		yyval = new SemValue();
                		yyval.elist = new ArrayList<Tree.Expr>();
                	}
break;
case 76:
//#line 368 "Parser.y"
{
						yyval.elist.add(val_peek(0).expr);
					}
break;
case 77:
//#line 372 "Parser.y"
{
                		yyval.elist = new ArrayList<Tree.Expr>();
						yyval.elist.add(val_peek(0).expr);
                	}
break;
case 78:
//#line 379 "Parser.y"
{
						yyval.stmt = new Tree.WhileLoop(val_peek(2).expr, val_peek(0).stmt, val_peek(4).loc);
					}
break;
case 79:
//#line 385 "Parser.y"
{
                        yyval.stmt = new Tree.RepeatLoop(val_peek(4).stmt, val_peek(1).expr, val_peek(5).loc);
                    }
break;
case 80:
//#line 391 "Parser.y"
{
						yyval.stmt = new Tree.ForLoop(val_peek(6).stmt, val_peek(4).expr, val_peek(2).stmt, val_peek(0).stmt, val_peek(8).loc);
					}
break;
case 81:
//#line 397 "Parser.y"
{
						yyval.stmt = new Tree.Break(val_peek(0).loc);
					}
break;
case 82:
//#line 403 "Parser.y"
{
						yyval.stmt = new Tree.If(val_peek(3).expr, val_peek(1).stmt, val_peek(0).stmt, val_peek(5).loc);
					}
break;
case 83:
//#line 409 "Parser.y"
{
						yyval.stmt = val_peek(0).stmt;
					}
break;
case 84:
//#line 413 "Parser.y"
{
						yyval = new SemValue();
					}
break;
case 85:
//#line 419 "Parser.y"
{
						yyval.stmt = new Tree.Return(val_peek(0).expr, val_peek(1).loc);
					}
break;
case 86:
//#line 423 "Parser.y"
{
                		yyval.stmt = new Tree.Return(null, val_peek(0).loc);
                	}
break;
case 87:
//#line 429 "Parser.y"
{
						yyval.stmt = new Print(val_peek(1).elist, val_peek(3).loc);
					}
break;
//#line 1191 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    //if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      //if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        //if (yychar<0) yychar=0;  //clean, if necessary
        //if (yydebug)
          //yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      //if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
//## The -Jnorun option was used ##
//## end of method run() ########################################



//## Constructors ###############################################
//## The -Jnoconstruct option was used ##
//###############################################################



}
//################### END OF CLASS ##############################
